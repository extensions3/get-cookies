const parseCookie = str =>
  str
    .split(';')
    .map(v => v.split('='))
    .reduce((acc, v) => {
      if (v.length === 1) acc['Noname'] = decodeURIComponent(v[0].trim());
      else acc[decodeURIComponent(v[0].trim())] = decodeURIComponent(v[1].trim());
      return acc;
    }, {});

const doShark = (element, times, distance, speed) => {
  for (i = 0; i < times; i++) {
    element.animate({marginLeft: '-=' + distance + 'px'}, speed)
      .animate({marginLeft: '+=' + distance + 'px'}, speed);
  }
}

const onCopyClick = (e) => {
  const cookieWrapper = `bearer ${e.target.parentNode.value}`;
  const el = document.createElement('textarea');
  el.value = cookieWrapper;
  document.body.appendChild(el);
  el.select();
  document.execCommand('copy');
  document.body.removeChild(el);
  doShark($(`#${e.target.id}`).parent(), 3, '5', 100);

}
const writeOntoPopup = (key, val, type) => {
  const result = `<div class="row row-cols-2 mb-1">
                    <div class="col-10">${key}</div>
                    <div class="col-2">
                      <button type="button" value="${val}" class="btn btn-link p-0">
                         <i id="${key}" class="fa fa-copy ${type} p-2"></i>
                      </button>
                    </div>
                  </div>`;
  return result
}
chrome.tabs.executeScript({
  code: 'document.cookie'
}, response => {
  const cookies = response[0];
  const objCookies = parseCookie(cookies);
  const arrCookies = Object.keys(objCookies);

  const uiCookies = arrCookies.map((key, keyInd) => {
    return writeOntoPopup(key, objCookies[key], 'cookies');
  })

  document.getElementById("cookies").innerHTML += uiCookies.join('');
  $(".cookies").click(onCopyClick);
});


chrome.tabs.executeScript({
  code: `
 function allStorage() {

    var archive = {}, // Notice change here
        keys = Object.keys(localStorage),
        i = keys.length;

    while ( i-- ) {
        archive[ keys[i] ] = localStorage.getItem( keys[i] );
    }

    return archive;
}
allStorage();
`
}, response => {

  const type = 'localStorage';
  const objData = response[0];
  const arrData = Object.keys(objData)
  const uiData = arrData.map((key) => writeOntoPopup(key, objData[key], type)
  )

  document.getElementById(type).innerHTML += uiData.join('');
  $(`.${type}`).click(onCopyClick);
});


