# GET COOKIES
This extension will get all cookies in active tab

## How to maintain

 1. Maintain UI 
   - Edit `popup.html` file.

 2. Maintain Functional
   - Edit `popup.js` file

## How to install

1. Open the Extension Management page by navigating to [chrome://extensions](chrome://extensions).
    - The Extension Management page can also be opened by clicking on the Chrome menu, hovering over **More Tools** then selecting **Extensions**.

2. Enable Developer Mode by clicking the toggle switch next to **Developer mode**.
3. Click the **LOAD UNPACKED** button and select the extension directory.
![Alt Text](https://developer.chrome.com/static/images/get_started/load_extension.png)



## How to use
Just click copy (^_^)

![Alt Text](https://camo.githubusercontent.com/a11579935891f044bcf8d1f7fedd385f30d95fd814d2cc0833d73e50da82396e/687474703a2f2f692e696d6775722e636f6d2f76384956446b612e6a7067)

## Release

***Version 2.0.0***

Get local storage and copy to clipboard

***Version 1.0.0***

Get cookies